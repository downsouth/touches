import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.KeyboardEvent;
import openfl.Lib;
import openfl.system.Capabilities;
import es.downsouth.touches.controllers.GameController;

class Main extends Sprite {
  
  private var Game:GameController;
  
  public function new() {
    super();
    this.addEventListener(Event.ADDED_TO_STAGE, added);
  }
  
  private function added(event:Event):Void {
    Game = new GameController();
    addChild(Game.view);
    
    Game.start();

    this.removeEventListener(Event.ADDED_TO_STAGE, added);
    #if android
    Lib.current.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
    #end
    Lib.current.stage.addEventListener(Event.RESIZE, onResize);

    var screenDensity:Float = getScreenDensity();
    resize(Math.round(screenDensity*Lib.current.stage.stageWidth), Math.round(screenDensity*Lib.current.stage.stageHeight));
  }

  private function getScreenDensity():Float {
    var screenDensity:Float = 1, dpi:Float = Capabilities.screenDPI;
    
    if (dpi < 200) {
      screenDensity = 1;
    } else if (dpi < 300) {
      screenDensity = 1.5;
    } else {
      screenDensity = 2;
    }
    return screenDensity;
  }

  private function resize(newWidth:Int, newHeight:Int):Void {
    Game.resize(newWidth, newHeight);
  }
  
  
  private function onKeyUp(event:KeyboardEvent):Void {
    #if android
    if (event.keyCode == 27) {
      event.stopImmediatePropagation();
      Sys.exit(0);
    }    
    #end
  }
  
  private function onResize(event:Event):Void {
    resize(stage.stageWidth, stage.stageHeight);  
  }
  
}