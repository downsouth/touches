package es.downsouth.touches.views;

import openfl.Lib;
import openfl.display.Bitmap;
import openfl.display.Sprite;
#if (mobile || html5)
import openfl.ui.Multitouch;
import openfl.ui.MultitouchInputMode;
import openfl.events.TouchEvent;
#end
#if !mobile
import openfl.events.MouseEvent;
#end
import openfl.geom.Point;
import openfl.display.FPS;
import openfl.text.TextField;
import openfl.text.TextFieldType;
import openfl.text.TextFormat;
import openfl.text.TextFieldAutoSize;
import openfl.text.TextFormatAlign;
import openfl.text.AntiAliasType;
import openfl.Assets;
import es.downsouth.touches.events.CircleEvent;
import es.downsouth.touches.views.CircleView;

class GameView extends Sprite {
  
  private var background:Sprite;
  public var currentScale:Float;
  public var currentScore:Int;
  private var circles:Array<CircleView>;
  private var lastPos:Point;
  private var fps:FPS;
  private var colors:Array<UInt>;
  private var colorIndexes:Map<UInt, UInt>;
#if (mobile || html5)
  private var multiTouchSupported:Bool;
#end

  public function new() {
    super();

    currentScale = 1;
    currentScore = 0;

    colorIndexes = new Map<UInt, UInt>();

    initColors();

    circles = new Array<CircleView>();

  #if (mobile || html5)
    multiTouchSupported = Multitouch.supportsTouchEvents;
    if(multiTouchSupported)
    {
      Multitouch.inputMode=MultitouchInputMode.TOUCH_POINT;
    }
    addEventListener(TouchEvent.TOUCH_BEGIN, onTouchEvent);
    addEventListener(TouchEvent.TOUCH_MOVE, onTouchEvent);
    addEventListener(TouchEvent.TOUCH_END, onTouchEvent);
  #end
  #if !mobile
    addEventListener(MouseEvent.MOUSE_DOWN, onMouseEvent);
    addEventListener(MouseEvent.MOUSE_MOVE, onMouseEvent);
    addEventListener(MouseEvent.MOUSE_UP, onMouseEvent);
  #end
    addEventListener(CircleEvent.DESTROY, onCircleEvent);
  }

  public function reset():Void {
    if(background != null){
      removeBackground();
    }
    addBackground();
    removeFPS();
    addFPS();
    lastPos = null;
  }

  private function addBackground():Void{
    background = new Sprite();
    background.x = 0;
    background.y = 0;
    drawBackground(width, height);
    addChild(background);
  }

  private function drawBackground(width:Float, height:Float):Void {
    background.graphics.clear();
    background.graphics.beginFill(0x000000, 1.0);
    background.graphics.drawRect(0, 0, width, height);
    background.graphics.endFill();
  }

  private function removeBackground():Void{
    removeChild(background);
    background = null;
  }

  public function resize(newWidth:Int, newHeight:Int):Void {
    drawBackground(newWidth, newHeight);
  }

#if (mobile || html5)
  private function onTouchEvent(e:TouchEvent){
    #if ios
      var pressureAmplification:UInt = 40;
    #else
      var pressureAmplification:UInt = 20;
    #end
    var touchStrokeFinished:Bool = (e.type == TouchEvent.TOUCH_END);
    addCircleAtPoint(new Point(e.stageX, e.stageY), (e.pressure * (1+e.pressure) * pressureAmplification), e.touchPointID, touchStrokeFinished);
    if(touchStrokeFinished){
      if(colorIndexes.exists(e.touchPointID)){
        // Removed previously defined index when the touch stops
        colorIndexes.remove(e.touchPointID);
      }
    }
  }
#end
#if !mobile
  private function onMouseEvent(e:MouseEvent){
    addCircleAtPoint(new Point(e.stageX, e.stageY), 50, 0, (e.type == MouseEvent.MOUSE_UP));
  }
#end

  private function onCircleEvent(e:CircleEvent){
    e.stopPropagation();
    var c:CircleView = e.data;
    circles.remove(c);
    removeChild(c);
    c = null;
  }

  private function addCircleAtPoint(p:Point, r:Float, touchPoint:UInt, strokeEnd:Bool=false):Void {
    var c:CircleView = new CircleView(r, getColor(touchPoint));
    addChild(c);
    if(lastPos == null){
      c.x = p.x-(c.width/2);
      c.y = p.y-(c.height/2);
    }else{
      c.x = lastPos.x-(c.width/2);
      c.y = lastPos.y-(c.height/2);
    }
    lastPos = p;
    circles.push(c);
    if(strokeEnd){
      c.moveAndDestroy(new Point(p.x-(c.width/2), p.y-(c.height/2)), "EXPLODE");
    }else{
      c.moveAndDestroy(new Point(p.x-(c.width/2), p.y-(c.height/2)));
    }
  }

  private function initColors():Void {
    colors = new Array<UInt>();
    var cMax:UInt = 256;
    // Skip the darkest colors
    var cMin:UInt = 50;

    // Set initial state
    var r:UInt = cMin;
    var g:UInt = cMin;
    var b:UInt = cMax;

    // Build RGB color array
    for(i in 1...(cMax-cMin)){
      colors.push(getRGBColor(r, g, b));
      r = i+cMin;
      b = cMax-i;
    }
    for(i in 1...(cMax-cMin)){
      colors.push(getRGBColor(r, g, b));
      g = i+cMin;
      r = cMax-i;
    }
    for(i in 1...(cMax-cMin)){
      colors.push(getRGBColor(r, g, b));
      b = i+cMin;
      g = cMax-i;
    }
  }

  private function getRGBColor(r:UInt, g:UInt, b:UInt):UInt{
    return (r << 16)+(g << 8)+b;
  }

  private function getColor(touchPoint:UInt):UInt {
    if(!colorIndexes.exists(touchPoint)){
      colorIndexes[touchPoint] = 0;
    }
    if(colorIndexes[touchPoint] >= colors.length){
      colorIndexes[touchPoint] = Std.int(Math.max(Math.min((colorIndexes[touchPoint] - colors.length), colors.length-1), 0));
    }
    var color:UInt = colors[colorIndexes[touchPoint]];
    colorIndexes[touchPoint] += Math.ceil(Math.random()*10);
    return color;
  }

  private function removeFPS():Void {
    if(fps != null){
      removeChild(fps);
      fps = null;
    }
  }

  private function addFPS():Void {
    fps = new FPS();
    fps.textColor = 0xFFFFFF;
    fps.autoSize = TextFieldAutoSize.RIGHT;
    fps.type = TextFieldType.DYNAMIC;
    fps.antiAliasType = AntiAliasType.ADVANCED;

    var textFormat = new TextFormat(Assets.getFont("fonts/OpenSans-Regular.ttf").fontName);
    textFormat.size = 22;
    textFormat.color = 0xFFFFFF;
    textFormat.letterSpacing = 1.5;
    textFormat.align = TextFormatAlign.CENTER;

    fps.defaultTextFormat = textFormat;
    fps.autoSize = TextFieldAutoSize.LEFT;
    fps.selectable = false;

    addChild(fps);
    fps.x = 0;
    fps.y = 0;
  }

  public function dispose():Void {
  #if (mobile || html5)
    removeEventListener(TouchEvent.TOUCH_BEGIN, onTouchEvent);
    removeEventListener(TouchEvent.TOUCH_MOVE, onTouchEvent);
    removeEventListener(TouchEvent.TOUCH_END, onTouchEvent);
 #end
  #if !mobile
    removeEventListener(MouseEvent.MOUSE_DOWN, onMouseEvent);
    removeEventListener(MouseEvent.MOUSE_MOVE, onMouseEvent);
    removeEventListener(MouseEvent.MOUSE_UP, onMouseEvent);
  #end
    removeEventListener(CircleEvent.DESTROY, onCircleEvent);
    removeFPS();
    if(circles.length > 0){
      for(c in circles){
        removeChild(c);
      }
    }
    circles = null;
    removeBackground();
  }
    
}