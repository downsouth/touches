package es.downsouth.touches.views;

import motion.Actuate;
import openfl.display.Sprite;
import openfl.display.GradientType;
import openfl.geom.Point;
import openfl.geom.Matrix;
import haxe.Timer;
import es.downsouth.touches.events.CircleEvent;

class CircleView extends Sprite {
  private var radius:Float = 50;
  private var diameter:Float = 100;
  private var color:UInt;
  private var pressure:Float;

  public function new(radius:Float, color:UInt) {
    super();
    this.radius = radius;
    this.diameter = radius*2;
    this.color = color;
    draw(radius);
  }

  private function draw(radius:Float):Void {
    var matrix:Matrix = new Matrix();
    matrix.createGradientBox(radius * 2, radius * 2);
    graphics.beginGradientFill(GradientType.RADIAL, [color, color], [1.0, 0.0], [0, 255], matrix);
    graphics.drawCircle(radius, radius, radius);
    graphics.endFill();
  }

  public function moveAndDestroy(p:Point, effect:String="FADE") {
    if(effect == "EXPLODE"){
      var explosionRadius:Float = radius*10;
      var explosionDiameter:Float = explosionRadius*2;
      Actuate.tween(this, 0.5, { x: p.x-explosionRadius+radius, y: p.y-explosionRadius+radius, width: explosionDiameter, height: explosionDiameter });
      Actuate.tween(this, 2.5, { alpha: 0 }).onComplete(onAnimationCompleted);
    }else{
      Actuate.tween(this, 0.5, { x: p.x, y: p.y }).onComplete(fadeAndDestroy);
    }
  }

  // private function implodeAndDestroy(){
  //   var implosionRadius:Float = 0/10;
  //   var implosionDiameter:Float = implosionRadius*2;
  //   var relativeXAdjustment:Float = x+(width/2);
  //   var relativeYAdjustment:Float = y+(height/2);
  //   Actuate.tween(this, 1, { x: relativeXAdjustment, y: relativeYAdjustment, width: implosionDiameter, height: implosionDiameter, alpha: 0 }).onComplete(onAnimationCompleted);
  // }

  private function fadeAndDestroy(){
    Actuate.tween(this, 2, { alpha: 0 }).delay(1).onComplete(onAnimationCompleted);
  }

  private function onAnimationCompleted(){
    var e:CircleEvent = new CircleEvent(CircleEvent.DESTROY);
    e.data = this;
    dispatchEvent(e);
  }

}