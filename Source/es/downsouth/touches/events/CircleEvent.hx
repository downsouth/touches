package es.downsouth.touches.events;

import openfl.events.Event;
import es.downsouth.touches.views.CircleView;

class CircleEvent extends Event {
  public static var DESTROY:String = "CircleEvent.DESTROY";
  public var data:CircleView;
  
  public function new(type:String) {
    super(type, true);
  }
}