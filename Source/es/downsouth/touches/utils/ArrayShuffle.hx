package es.downsouth.memory.utils;

class ArrayShuffle {

  // Returns a new array with the elements shuffled.
  // Uses the Durstenfeld algorithm.
  // http://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#The_modern_algorithm
  public static function shuffle<T>(source:Array<T>):Array<T> {
    var a:Array<T> = source.slice(0);
    for (i in 1...a.length) {
      var j:Int = Std.random(i+1);
      a[i] = a[j];
      a[j] = source[i];
    }
    return a;
  }

}