package es.downsouth.touches.signals;

import msignal.Signal;

class GameSignal extends Signal1<String>
{
  public static inline var RENDERED:String = "TouchesSignal.RENDERED";

  public function new()
  {
    super(String);
  }
}
