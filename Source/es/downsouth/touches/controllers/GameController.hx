package es.downsouth.touches.controllers;

import openfl.geom.Point;
import es.downsouth.touches.views.GameView;

class GameController {

  public var view:GameView;

  public function new() {
    view = new GameView();
  }

  public function start(){
    view.reset();
  }

  public function resize(newWidth:Int, newHeight:Int):Void {
    view.resize(newWidth, newHeight);
  }

  public function dispose():Void {
    view.dispose();
  }

}