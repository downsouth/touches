Touches
===========

Multitouch OpenFL demo. Tested with OpenFL 4.x and Haxe 3.x on HTML5, iOS, Android, macOS and Ubuntu.

Demo hosted at https://apps.localhost3000.es/touches/index.html and https://ipfs.io/ipfs/QmX811Cy81QN1JbPeUXoa96e2MHwDaAKfRWd8srtH72fbL/index.html.